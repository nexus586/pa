import { Injectable } from '@angular/core';
import { Http, Headers, Response } from "@angular/http";
import { Observable } from "rxjs/Observable";
import { Router } from "@angular/router";
import 'rxjs/Rx';

@Injectable()
export class StudentService {

  token = localStorage.getItem('token');
  id  = localStorage.getItem('user');

  headers = new Headers({'Content-Type':'application/json', 'Authorization':this.token})


  constructor(private http: Http, private router: Router) { }

  getAllStudents(){
    return this.http.get(`http://localhost:8080/payapp/parent/getChildren?id=${this.id}`, {headers: this.headers})
                    .map(response => response.json())
                    .catch(error => Observable.throw(error));
  }

  getStudentById(id: number){
    return this.http.get(`http://localhost:8080/payapp/parent/getChildById?childId=${id}`, {headers: this.headers})
                    .map(response => response.json())
                    .catch(error => Observable.throw(error));
  }

  addStudent(form: any){
    return this.http.post(`http://localhost:8080/payapp/parent/addNewChild?id=${this.id}`, form, {headers: this.headers})
                    .map(response => response.json())
                    .catch(error => Observable.throw(error));
  }

  getAllSchools(){
    return this.http.get('http://localhost:8080/payapp/user/getAllSchools', {headers: this.headers})
                    .map(response => response.json())
                    .catch(error => Observable.throw(error));
  }

}
