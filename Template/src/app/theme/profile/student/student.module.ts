import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StudentRoutingModule } from './student-routing.module';
import { ViewStudentsComponent } from './view-students/view-students.component';
import { AddStudentComponent } from './add-student/add-student.component';
import { ViewStudentComponent } from './view-student/view-student.component';
import { SharedModule } from './../../../shared/shared.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { SelectModule } from 'ng-select';
import { StudentService } from './student.service';
import { HttpModule } from '@angular/http';
import {  SimpleNotificationsModule  } from 'angular2-notifications';


@NgModule({
  imports: [
    CommonModule,
    StudentRoutingModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule,
    HttpModule,
    SimpleNotificationsModule.forRoot()
  ],
  declarations: [ViewStudentsComponent, AddStudentComponent, ViewStudentComponent],
  providers: [StudentService]
})
export class StudentModule { }
