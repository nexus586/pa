import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { StudentService } from '../student.service';
import { NotificationsService } from 'angular2-notifications';
import { AppConstants } from '../../../../shared/AppConstants';

@Component({
  selector: 'app-view-students',
  templateUrl: './view-students.component.html',
  styleUrls: ['./view-students.component.scss',
    '../../../../../assets/icon/icofont/css/icofont.scss'
  ]
})
export class ViewStudentsComponent implements OnInit {

  students: any[];
  loading: boolean;
  length: number;

  notiOptions = AppConstants.notificationConstants;

  constructor(private router: Router, private service: StudentService, private noti: NotificationsService) { }

  ngOnInit() {
    this.loading = true;

    this.service.getAllStudents().subscribe(
      data => {
        this.loading = !this.loading;
        this.students = data;
        this.length = this.students.length;
      },
      error => {
        this.loading = !this.loading;
        this.noti.error('Error', 'Unable to fetch data');
        console.log(error);
      }
    );
  }

  goToAdd(): void {
    this.router.navigate(['/', 'student', 'add-student']);
  }

}
