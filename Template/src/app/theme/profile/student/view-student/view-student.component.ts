import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { StudentService } from '../student.service';
import { FormControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Location } from '@angular/common';

@Component({
  selector: 'app-view-student',
  templateUrl: './view-student.component.html',
  styleUrls: ['./view-student.component.scss']
})
export class ViewStudentComponent implements OnInit {

  student: any = {};
  id: number;
  isLoading: boolean;

  editStudent = true;
  editStudentIcon: any = 'fa-edit';

  ret: any = {};

  child: FormGroup;

  firstName: FormControl = new FormControl('', Validators.required);
  lastName: FormControl = new FormControl('', Validators.required);
  age: FormControl = new FormControl('', [Validators.required, Validators.maxLength(2)]);
  grade: FormControl = new FormControl('', Validators.required);
  school: FormControl = new FormControl('', Validators.required);

  constructor(private r: ActivatedRoute,
    private router: Router,
    private service: StudentService,
    private fb: FormBuilder,
    private location: Location) { }

  ngOnInit() {

    this.child = this.fb.group({
      firstName: this.firstName,
      lastName: this.lastName,
      age: this.age,
      grade: this.grade,
      school: this.school
    });

    this.isLoading = true;
    this.id = +this.r.snapshot.params['id'];

    this.service.getStudentById(this.id).subscribe(
      (data) => {
        this.student = data;
        this.isLoading = !this.isLoading;
      },
      (error) => {
        console.log('error');
        this.isLoading = !this.isLoading;
      }
    );
  }

  toggleEditStudent(): void {
    this.editStudentIcon = (this.editStudentIcon === 'fa-close') ? 'fa-edit' : 'fa-close';
    this.editStudent = !this.editStudent;
  }

  update(): void {
    console.log('update has been called');
  }

  goBack(): void {
    this.location.back();
  }

}
