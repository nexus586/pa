import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormBuilder, FormGroup, FormsModule } from '@angular/forms';
import { Router } from '@angular/router';
import { NotificationsService } from 'angular2-notifications';
import { StudentService } from '../student.service';
import { AppConstants } from '../../../../shared/AppConstants';


@Component({
  selector: 'app-add-student',
  templateUrl: './add-student.component.html',
  styleUrls: ['./add-student.component.scss']
})
export class AddStudentComponent implements OnInit {
  student: any;
  schools: any[];
  form: FormGroup;

  firstName: FormControl = new FormControl('', Validators.required);
  lastName: FormControl = new FormControl('', Validators.required);
  // age: FormControl = new FormControl('', [Validators.required, Validators.maxLength(2)]);
  dobString: FormControl = new FormControl('', Validators.required);
  grade: FormControl = new FormControl('', Validators.required);
  school: FormControl = new FormControl('', Validators.required);

  notiOptions = AppConstants.notificationConstants;

  constructor(private fb: FormBuilder, private r: Router, private service: StudentService, private noti: NotificationsService) { }

  ngOnInit() {

    this.service.getAllSchools().subscribe(
      (data) => {
        this.schools = data;
      },
      (error) => {
        console.log('error');
      }
    );

    this.form = this.fb.group({
      firstName: this.firstName,
      lastName: this.lastName,
      dobString: this.dobString,
      grade: this.grade,
      school: this.school
    });
  }

  submit(): void {
    this.noti.info('Adding', 'Please wait while we add your child', {timeOut: 1000});

    this.student = {
      firstName: this.form.value.firstName,
      lastName: this.form.value.lastName,
      age: this.form.value.age,
      grade: this.form.value.grade,
      dobString: this.form.value.dobString,
      schoolName: this.form.value.school
    };


    this.service.addStudent(JSON.stringify(this.student)).subscribe(
      (data) => {
        this.noti.success(data.status, data.reason);
        setTimeout(() => {
          this.r.navigate(['student', 'view-students']);
        }, 2000);
      },
      (error) => {
        this.noti.error(error.status, error.reason);
      }
    );
  }

}
