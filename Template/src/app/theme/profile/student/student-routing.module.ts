import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AddStudentComponent } from './add-student/add-student.component';
import { ViewStudentComponent } from './view-student/view-student.component';
import { ViewStudentsComponent } from './view-students/view-students.component';

const routes: Routes = [
  {
    path: 'view-students',
    component: ViewStudentsComponent,
    data: {
      title: 'My Children',
      icon: 'icon-layout-sidebar-left',
      caption: 'Here"s a list of your Children',
      status: true
    }
  },
  {
    path: 'add-student',
    component: AddStudentComponent,
    data: {
      title: 'Add A Child',
      icon: 'icon-layout-sidebar-left',
      caption: 'Add one of your children here',
      status: true
    }
  },
  {
    path: 'id/:id',
    component: ViewStudentComponent,
    data: {
      title: 'Student Info',
      icon: 'icon-layout-sidebar-left',
      caption: 'Full Info about the student',
      status: true
    }
  }

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class StudentRoutingModule { }
