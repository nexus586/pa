import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: 'account',
    loadChildren: './account/account.module#AccountModule',
    data: {
      title: 'Account',
      status: false
    }
  },
  {
    path: 'student',
    loadChildren: './student/student.module#StudentModule',
    data: {
      title: 'Student',
      status: false
    }
  },
  {
    path: 'payment',
    loadChildren: './transactions/transactions.module#TransactionsModule',
    data: {
      title: 'Payments',
      status: false
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProfileRoutingModule { }
