import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TransactionsRoutingModule } from './transactions-routing.module';
import { ViewTransactionsComponent } from './view-transactions/view-transactions.component';
import { MakePaymentComponent } from './make-payment/make-payment.component';
import { ViewTransactionComponent } from './view-transaction/view-transaction.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SelectModule } from 'ng-select';
import { SharedModule } from '../../../shared/shared.module';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { AccountService } from '../account/account.service';
import { StudentService } from '../student/student.service';
import { TransactionService } from './transaction.service';
import { SimpleNotificationsModule } from 'angular2-notifications';

@NgModule({
  imports: [
    CommonModule,
    TransactionsRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    SelectModule,
    SharedModule,
    NgxDatatableModule,
    SimpleNotificationsModule.forRoot()
  ],
  declarations: [ViewTransactionsComponent, MakePaymentComponent, ViewTransactionComponent],
  providers: [AccountService, StudentService, TransactionService]
})
export class TransactionsModule { }
