import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { MakePaymentComponent } from './make-payment/make-payment.component';
import { ViewTransactionComponent } from './view-transaction/view-transaction.component';
import { ViewTransactionsComponent } from './view-transactions/view-transactions.component';

const routes: Routes = [
  {
    path: 'view-payments',
    component: ViewTransactionsComponent,
    data: {
      title: 'Transaction Receipts',
      icon: 'icon-layout-sidebar-left',
      caption: 'List of Transactions',
      status: true
    }
  },
  {
    path: 'make-payment',
    component: MakePaymentComponent,
    data: {
      title: 'Make Payment',
      icon: 'icon-layout-sidebar-left',
      caption: 'Pay Fees Here',
      status: true
    }
  },
  {
    path: 'id/:id',
    component: ViewTransactionComponent,
    // data: {
    //   title: 'Transaction Details',
    //   icon: 'icon-layout-sidebar-left',
    //   // caption: 'lorem ipsum dolor sit amet, consectetur adipisicing elit - sample page',
    //   status: true
    // }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TransactionsRoutingModule { }
