import { Component, OnInit } from '@angular/core';
import { TransactionService } from '../transaction.service';
import { NotificationsService } from 'angular2-notifications';
import { AppConstants } from '../../../../shared/AppConstants';
import {Title} from '@angular/platform-browser';
import {Router, NavigationEnd, ActivatedRoute} from '@angular/router';
import { BreadcrumbsComponent } from '../../../../layout/admin/breadcrumbs/breadcrumbs.component';

@Component({
  selector: 'app-view-transactions',
  templateUrl: './view-transactions.component.html',
  styleUrls: ['./view-transactions.component.scss']
})
export class ViewTransactionsComponent implements OnInit {

  trans: any[];
  loading: boolean;
  role: string;
  length: number;
  notiOptions = AppConstants.notificationConstants;
  mIsSchool: boolean;
  mIsParent: boolean;

  constructor(private service: TransactionService,
    private notiService: NotificationsService,
    private ts: Title,
    private router: Router,
    private actRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    this.loading = true;
    this.role = JSON.parse(localStorage.getItem('roles'));

    if (this.isParent(this.role)) {
      this.mIsParent = true;
      this.service.getAllParentTransactions().subscribe(
        (data) => {
          this.length = data.length;
          this.loading = !this.loading;
          this.trans = data;
        },
        (error) => {
          this.loading = !this.loading;
          this.notiService.error('Error', 'Unable to fetch data');
        }
      );
    }else if (this.isSchool(this.role)) {
      this.mIsSchool = true;
      this.service.getAllSchoolTransactions().subscribe(
        (data) => {
          this.length = data.length;
          this.loading = !this.loading;
          this.trans = data;
        },
        (error) => {
          this.loading = !this.loading;
          this.notiService.error('Error', 'Unable to fetch data');
        }
      );
    }

    if (this.isSchool(this.role)) {
      const title = 'My Receipts';
      this.ts.setTitle(title + ' | PayApp');
    }

  }

  private isSchool(role): boolean {
    return role === 'ROLE_SCHOOL';
  }

  private isParent(role): boolean {
    return role === 'ROLE_PARENT';
  }

}
