import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
// import 'rxjs/Rx';

@Injectable()
export class TransactionService {

  token = localStorage.getItem('token');
  id = localStorage.getItem('user');

  headers = new Headers({ 'Content-Type': 'application/json', 'Authorization': this.token });


  constructor(private router: Router, private http: Http) { }

  makePayment(form: any) {
    return this.http.post(`http://localhost:8080/payapp/parent/receiveMoMoPayment?id=${this.id}`, form, { headers: this.headers })
      .map(response => response.json())
      .catch(error => error.json());
  }

  getAllParentTransactions() {
    return this.http.get(`http://localhost:8080/payapp/parent/getAllTransactions?id=${this.id}`, { headers: this.headers })
      .map((response) => response.json())
      .catch(error => Observable.throw(error));
  }

  getAllSchoolTransactions() {
    return this.http.get(`http://localhost:8080/payapp/school/viewAllTransactions?id=${this.id}`, { headers: this.headers })
      .map(response => response.json())
      .catch(error => Observable.throw(error));
  }

  findTransactionById(id: number) {
    return this.http.get(`http://localhost:8080/payapp/user/viewTransactionById?id=${id}`, {headers: this.headers})
                    .map(response => response.json())
                    .catch(error => Observable.throw(error));
  }
}
