import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormBuilder, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AccountService } from '../../account/account.service';
import { StudentService } from '../../student/student.service';
import swal from 'sweetalert2';
import { TransactionService } from '../transaction.service';
import { NotificationsService } from 'angular2-notifications';
import { AppConstants } from '../../../../shared/AppConstants';

@Component({
  selector: 'app-make-payment',
  templateUrl: './make-payment.component.html',
  styleUrls: ['./make-payment.component.scss']
})
export class MakePaymentComponent implements OnInit {

  constructor(private fb: FormBuilder,
    private r: Router,
    private ac: AccountService,
    private ss: StudentService,
    private ts: TransactionService,
    private ns: NotificationsService
  ) { }

  form: any;

  notiOptions = AppConstants.notificationConstants;

  children: any[];
  accounts: any[];

  selectedChild: any;

  isLoading = true;


  child = new FormControl('', Validators.required);
  amount = new FormControl('', Validators.required);
  account = new FormControl('', Validators.required);
  trans: any;

  ngOnInit() {
    this.form = this.fb.group({
      cId: this.child,
      amount: this.amount,
      pAccountId: this.account
    });

    this.ac.viewAccounts().subscribe(
      (data) => {
        this.accounts = data;
        // console.log(this.accounts)
      },
      (error) => {
        console.log(error);
      }
    );

    this.ss.getAllStudents().subscribe(
      (data) => {
        this.children = data;
        this.isLoading = !this.isLoading;
        // console.log(this.children)
      },
      (error) => {
        console.log(error);
      }
    );

  }

  makePayment(childId: number, amount: number, accountId: number): void {
    if (childId.toString() === '' || amount.toString() === '' || accountId.toString() === '') {
      swal({
        title: 'Error',
        type: 'error',
        text: 'Required fields are empty'
      });
      return;
    } else if (amount < 0) {
      swal({
        title: 'Error',
        type: 'error',
        text: 'Amount Cannot be less than 0'
      });
      return;
    }

    this.ns.info('Please Wait', 'Processing transaction', {timeOut: 1000});

    this.trans = {
      cId: childId,
      amount: amount,
      pAccountId: accountId
    };

    this.ts.makePayment(JSON.stringify(this.trans)).subscribe(
      (data) => {
        this.ns.success('Saved', 'Transaction has been saved');
        setTimeout(() => {
          this.r.navigate(['payment', 'view-payments']);
        }, 2000);
        console.log('[data] =>', data);
      },
      (error) => {
        this.ns.error('Error', 'An unexpected error occured. Please try again later');
        console.log('[error] =>', error);
      }
    );

    // console.log(JSON.stringify(this.trans));
  }
}
