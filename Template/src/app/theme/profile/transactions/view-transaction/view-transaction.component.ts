import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TransactionService } from '../transaction.service';
import { NotificationsService } from 'angular2-notifications';
import { AppConstants } from '../../../../shared/AppConstants';


@Component({
  selector: 'app-view-transaction',
  templateUrl: './view-transaction.component.html',
  styleUrls: ['./view-transaction.component.scss']
})
export class ViewTransactionComponent implements OnInit {

  id: number;
  transaction: any;
  isLoading: boolean;
  notiOptions = AppConstants.notificationConstants;

  constructor(private active: ActivatedRoute,
    private r: Router,
    private service: TransactionService,
    private noti: NotificationsService
  ) { }

  ngOnInit() {
    this.isLoading = true;
    this.id = +this.active.snapshot.params['id'];

    this.service.findTransactionById(this.id).subscribe(
      (data) => {
        this.isLoading = false;
        this.transaction = data;
      },
      (error) => {
        this.isLoading = false;
        this.noti.error('Error', 'An unexpected error occured. Please try again later');
        console.log(error);
      }
    );
  }

}
