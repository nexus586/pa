import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AddAccountComponent } from './add-account/add-account.component';
import { ViewAccountsComponent } from "./view-accounts/view-accounts.component";
import { ViewAccountComponent } from "./view-account/view-account.component";

const routes: Routes = [
  {
    path: 'add-account',
    component: AddAccountComponent,
    data: {
      title: 'Add an Account',
      icon: 'icon-layout-sidebar-left',
      caption: 'Add one of your Mobile Money Accounts Here',
      status: true
    }
  },{
    path: 'view-accounts',
    component: ViewAccountsComponent,
    data: {
      title: 'My Accounts',
      icon: 'icon-layout-sidebar-left',
      caption: "Here's a list of your accounts",
      status: true
    }
  },{
    path: 'id/:id',
    component: ViewAccountComponent,
    data: {
      title: 'Account Info',
      icon: 'icon-layout-sidebar-left',
      caption: "More info about the account",
      status: true
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccountRoutingModule { }
