import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import swal from 'sweetalert2';
import { NotificationsService } from 'angular2-notifications';
import { AccountService } from '../account.service';
import { AppConstants } from '../../../../shared/AppConstants';

@Component({
  selector: 'app-add-account',
  templateUrl: './add-account.component.html',
  styleUrls: [
    './add-account.component.scss',
    '../../../../../assets/icon/icofont/css/icofont.scss'
  ]
})
export class AddAccountComponent implements OnInit {

  providers: any[] = [
    {label: 'MTN', value: 'mtn'},
    {label: 'Vodafone', value: 'vodafone'},
    {label: 'Tigo', value: 'tigo'},
    {label: 'Airtel', value: 'airtel'}
  ];

  notiOptions: any = AppConstants.notificationConstants;

  form: FormGroup;

  account: any;

  isLoading: boolean;

  accountNumber = new FormControl('', Validators.required);
  accountName = new FormControl('', Validators.required);
  accountProvider = new FormControl('', Validators.required);

  constructor(private fb: FormBuilder, private router: Router, private noti: NotificationsService, private service: AccountService) { }

  ngOnInit() {
    this.form = this.fb.group({
      accountNumber: this.accountNumber,
      accountName: this.accountName,
      accountProvider: this.accountProvider
    });
  }

  onSubmit(): void {
    this.isLoading = true;
    this.account = {
      accountName: this.form.value.accountName,
      accountNumber: this.form.value.accountNumber,
      accountProvider: this.form.value.accountProvider
    };

    this.service.addAccount(JSON.stringify(this.account)).subscribe(
      (data) => {
        this.isLoading = !this.isLoading;
        this.noti.success(data.status, data.reason);
        setTimeout(() => {
          this.router.navigate(['account', 'view-accounts']);
        }, 2000);
      },
      (error) => {
        this.isLoading = !this.isLoading;
        this.noti.error(error.status, error.reason);
      }
    );

    // console.log('form is', this.account);
  }

}
