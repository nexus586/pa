import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
// import 'rxjs/Rx';


@Injectable()
export class AccountService {

  token = localStorage.getItem('token');
  id = localStorage.getItem('user');

  headers = new Headers({'Content-Type': 'application/json', 'Authorization': this.token});

  constructor(private http: Http, private router: Router) { }

  addAccount(account: any) {
    return this.http.post(`http://localhost:8080/payapp/user/addAccount?id=${this.id}`, account, {headers: this.headers})
                    .map(response => response.json())
                    .catch((error) => Observable.throw(error));
  }

  viewAccounts() {
    return this.http.get(`http://localhost:8080/payapp/user/getAllAccounts?id=${this.id}`, {headers: this.headers})
                    .map(response => response.json())
                    .catch(error => Observable.throw(error));
  }

  viewAccount(id: number) {
    return this.http.get(`http://localhost:8080/payapp/user/getAccountById?id=${id}`, {headers: this.headers})
                    .map(response => response.json())
                    .catch(error => Observable.throw(error));
  }

  removeAccount(id: number) {
    return this.http.delete(`http://localhose:8080/payapp/user/removeAccount?id=${id}`, {headers: this.headers})
                    .map(response => response.json())
                    .catch(error => Observable.throw(error));
  }

  editAccount(account: any) {
    return this.http.put(`http://localhost:8080/payapp/user/editAccount?id=${this.id}`, account, {headers: this.headers})
                    .map(response => response.json())
                    .catch(error => Observable.throw(error));
  }

}
