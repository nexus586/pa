import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { AccountService } from '../account.service';
import swal from 'sweetalert2';

@Component({
  selector: 'app-view-account',
  templateUrl: './view-account.component.html',
  styleUrls: ['./view-account.component.scss']
})
export class ViewAccountComponent implements OnInit {
  editAccount = true;
  editAccountIcon = 'fa-edit';
  id: number;

  isLoading: boolean;

  ret: any = {};

  account: FormGroup;

  accountName: FormControl = new FormControl('', Validators.required);
  accountNumber: FormControl = new FormControl('', Validators.required);
  accountProvider: FormControl = new FormControl('', Validators.required);

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private service: AccountService,
    private fb: FormBuilder,
    private location: Location) { }

  ngOnInit() {
    this.isLoading = true;

    this.id = +this.route.snapshot.params['id'];

    this.account = this.fb.group({
      accountName: this.accountName,
      accountNumber: this.accountNumber,
      accountProvider: this.accountProvider
    });

    this.service.viewAccount(this.id).subscribe(
      (data) => {
        this.ret = data;
        this.isLoading = !this.isLoading;
      },
      (error) => {
        this.isLoading = !this.isLoading;
        console.log(error);
      }
    );
  }

  toggleEditAccount(): void {
    this.editAccountIcon = (this.editAccountIcon === 'fa-close') ? 'fa-edit' : 'fa-close';
    this.editAccount = !this.editAccount;
  }

  update(): void {
    const form = {
      accountName: this.account.value.accountName,
      accountNumber: this.account.value.accountNumber,
      accountProvider: this.account.value.accountProvider
    };

    console.log('Form is: ', JSON.stringify(form));

    this.service.editAccount(JSON.stringify(form)).subscribe(
      (data) => {
        console.log('update success');
        this.router.navigate(['/', 'account', 'view-accounts']);
      },
      (error) => {
        console.log('error');
      }
    );
  }

  goBack(): void {
    this.location.back();
  }

  removeAccount(): void {
    swal({
      title: 'Delete Account',
      text: 'Are you sure you want to delete this account',
      type: 'warning',
      showConfirmButton: true,
      confirmButtonText: 'Delete',
      confirmButtonColor: 'red',
      showCancelButton: true,
      cancelButtonColor: 'green'
    }).then(
      (result) => {
        if (result.value) {
          this.service.removeAccount(this.id).subscribe(
            (data) => {
              this.router.navigate(['account', 'view-accounts']);
            },
            (error) => {
              swal('Error', 'Unable to delete account', 'error');
            }
          );
        }
      }
    );
  }
}
