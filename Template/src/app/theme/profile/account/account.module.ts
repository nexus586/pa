import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AccountRoutingModule } from './account-routing.module';
import { AddAccountComponent } from './add-account/add-account.component';
import { ViewAccountsComponent } from './view-accounts/view-accounts.component';
import { ViewAccountComponent } from './view-account/view-account.component';
import { SharedModule } from '../../../shared/shared.module';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { SelectModule } from 'ng-select';
import { AccountService } from './account.service';
import { HttpModule } from '@angular/http';
import {  SimpleNotificationsModule  } from 'angular2-notifications';


@NgModule({
  imports: [
    CommonModule,
    AccountRoutingModule,
    SharedModule,
    ReactiveFormsModule,
    FormsModule,
    SelectModule,
    HttpModule,
    SimpleNotificationsModule.forRoot()
  ],
  declarations: [AddAccountComponent, ViewAccountsComponent, ViewAccountComponent],
  providers: [AccountService]
})
export class AccountModule { }
