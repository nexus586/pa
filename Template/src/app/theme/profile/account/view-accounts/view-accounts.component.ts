import { Component, OnInit } from '@angular/core';
import { Router } from "@angular/router";
import { AccountService } from "../account.service";
import { NotificationsService } from "angular2-notifications";
import { AppConstants } from "../../../../shared/AppConstants";


@Component({
  selector: 'app-view-accounts',
  templateUrl: './view-accounts.component.html',
  styleUrls: [
    './view-accounts.component.scss',
    '../../../../../assets/icon/icofont/css/icofont.scss'
  ]
})
export class ViewAccountsComponent implements OnInit {

  accounts: any[];
  loading: boolean;
  length: number;

  notiOptions = AppConstants.notificationConstants;

  constructor(private router: Router, private service: AccountService, private noti: NotificationsService) { }

  ngOnInit() {
    this.loading = true;
    this.service.viewAccounts().subscribe(
      (data) =>{
        this.loading = !this.loading;
        this.accounts = data;
        this.length = this.accounts.length;
        // console.log('accounts[data]: ', this.accounts);
        // console.log('length: ', this.accounts.length);
      },
      (error) =>{
        // console.log("accounts[error]: ",this.accounts);
        this.loading = !this.loading;
        this.noti.error("Error", "Unable to fetch data");
        console.log(error);
      }
    )
  }

  goToAdd(): void{
    this.router.navigate(['account', 'add-account']);
  }
}
