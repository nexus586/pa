import { Component, OnInit } from '@angular/core';
import { animate, style, transition, trigger } from '@angular/animations';
import { AuthService } from "../../auth/auth.service";
import { NotificationsService } from "angular2-notifications";
import { AppConstants } from "../../../shared/AppConstants";


@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: [
    './user-profile.component.scss',
    '../../../../assets/icon/icofont/css/icofont.scss'
  ],
  animations: [
    trigger('fadeInOutTranslate', [
      transition(':enter', [
        style({ opacity: 0 }),
        animate('400ms ease-in-out', style({ opacity: 1 }))
      ]),
      transition(':leave', [
        style({ transform: 'translate(0)' }),
        animate('400ms ease-in-out', style({ opacity: 0 }))
      ])
    ])
  ]
})
export class UserProfileComponent implements OnInit {
  editProfile = true;
  editProfileIcon = 'icofont-edit';

  editAbout = true;
  editAboutIcon = 'icofont-edit';

  public editor;
  public editorContent: string;

  public data: any;
  public rowsOnPage = 10;
  public filterQuery = '';
  public sortBy = '';
  public sortOrder = 'desc';
  profitChartOption: any;

  loading: boolean = true;

  user: any;

  type: string;

  notiO = AppConstants.notificationConstants;

  rowsContact = [];
  loadingIndicator = true;
  role: string;
  reorderable = true;

  updatedUser: any;

  constructor(private auth: AuthService ,public noti: NotificationsService) { }
  id: number;

  ngOnInit() {
    this.id = JSON.parse(localStorage.getItem('user'));
    this.loading = true;
    this.role = JSON.parse(localStorage.getItem('roles'));

    this.type = this.role.substring(5, this.role.length);


    this.auth.findUser(this.id).subscribe(
      (data) => {
        this.loading = !this.loading;
        this.user = data;
      },
      (error) => {
        console.log(error);
      }
    )
  }

  update(name :string, email: string, phone: string){
    this.updatedUser = {
      name: name,
      email: email,
      phone: phone
    }
    this.auth.updateUser(this.id, this.updatedUser).subscribe(
      (data) =>{
        this.noti.success('Success', 'Profile updated successfully');
        setTimeout(()=>{
          window.location.reload();
        }, 2000)
      },
      (error) =>{
        this.noti.error('Error', 'An error occured. Please try again later');
      }
    )
  }

  toggleEditProfile() {
    this.editProfileIcon = (this.editProfileIcon === 'icofont-close') ? 'icofont-edit' : 'icofont-close';
    this.editProfile = !this.editProfile;
  }

  toggleEditAbout() {
    this.editAboutIcon = (this.editAboutIcon === 'icofont-close') ? 'icofont-edit' : 'icofont-close';
    this.editAbout = !this.editAbout;
  }

}
