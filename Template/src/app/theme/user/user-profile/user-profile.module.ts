import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { UserProfileComponent } from './user-profile.component';
import {UserProfileRoutingModule} from './user-profile-routing.module';
import {SharedModule} from '../../../shared/shared.module';
import {NgxDatatableModule} from '@swimlane/ngx-datatable';
import { AuthService } from "../../auth/auth.service";
import { SimpleNotificationsModule } from "angular2-notifications";


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    UserProfileRoutingModule,
    SharedModule,
    NgxDatatableModule, 
    SimpleNotificationsModule.forRoot()
  ],
  declarations: [UserProfileComponent],
  providers: [AuthService]
})
export class UserProfileModule { }
