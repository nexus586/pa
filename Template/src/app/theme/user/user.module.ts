import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {UserRoutingModule} from './user-routing.module';
import {SharedModule} from '../../shared/shared.module';
import { SimpleNotificationsModule } from "angular2-notifications";

@NgModule({
  imports: [
    CommonModule,
    UserRoutingModule,
    SharedModule,
    SimpleNotificationsModule.forRoot()
  ],
  declarations: []
})
export class UserModule { }
