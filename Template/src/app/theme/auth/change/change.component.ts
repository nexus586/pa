import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';
import { AppConstants } from '../../../shared/AppConstants';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { NotificationsService } from 'angular2-notifications';
import { CustomValidators } from 'ng2-validation';


@Component({
  selector: 'app-change',
  templateUrl: './change.component.html',
  styleUrls: ['./change.component.scss']
})
export class ChangeComponent implements OnInit {

  notiOptions = AppConstants.notificationConstants;

  form: FormGroup;

  email = new FormControl('', [Validators.required, Validators.email]);
  password = new FormControl('', Validators.required);
  confPassword = new FormControl('', [Validators.required, CustomValidators.equalTo(this.password)]);

  constructor(private service: AuthService, private fb: FormBuilder, private ns: NotificationsService) { }

  ngOnInit() {
    this.form = this.fb.group({
      email: this.email,
      password: this.password,
      confPassword: this.confPassword
    });
  }

  onSubmit() {

  }

}
