import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {AuthRoutingModule} from './auth-routing.module';
import { AuthService } from './auth.service';
import {SharedModule} from '../../shared/shared.module';
import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';

@NgModule({
  imports: [
    CommonModule,
    AuthRoutingModule,
    SharedModule,
    HttpClientModule,
    HttpModule
  ],
  // declarations: [ChangeComponent],
  providers: [AuthService]
})
export class AuthModule { }
