import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, Validators, FormGroup } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../../auth.service';
import { NotificationsService } from 'angular2-notifications';
import { AppConstants } from '../../../../shared/AppConstants';


@Component({
  selector: 'app-basic-login',
  templateUrl: './basic-login.component.html',
  styleUrls: [
    './basic-login.component.scss',
    '../../../../../assets/icon/icofont/css/icofont.scss'
  ]
})
export class BasicLoginComponent implements OnInit {

  email = new FormControl('', [Validators.required, Validators.email]);
  password = new FormControl('', Validators.required);
  login: any;

  infoOptions: any = AppConstants.notificationConstants;

  loginForm: FormGroup;
  constructor(private fb: FormBuilder, private router: Router, private service: AuthService, private notify: NotificationsService) { }

  ngOnInit() {
    this.loginForm = this.fb.group({
      email: this.email,
      password: this.password
    });
  }

  onSubmit() {
    this.notify.info('Logging In',
    'Please Wait while we log you in',
    {
      position: ['top', 'right'],
      animate: 'fromRight',
      timeOut: 2000,
      showProgressBar: false
    });

    this.login = {
      email: this.loginForm.value.email,
      password: this.loginForm.value.password
    };

    this.service.login(JSON.stringify(this.login))
      .subscribe(
      (data) => {
        const o = JSON.parse(JSON.stringify(data));

        // console.log("returned user is: " + JSON.stringify(data.user));

        localStorage.setItem('user', JSON.stringify(data.user));
        localStorage.setItem('name', data.name);
        localStorage.setItem('roles', JSON.stringify(data.roles[0].authority));
        localStorage.setItem('token', data.token);
        localStorage.setItem('isLoggedIn', 'true');
        this.notify.success('Success', 'You have been logged in successfully');
        setTimeout(
         () => {
          this.router.navigate(['/', 'user']);
         }, 2000);
        // localStorage.setItem('')
      },
      (error) => {
        this.loginForm.reset();
        console.log(JSON.stringify(error));
        if (error.status.toString().startsWith('5')) {
          this.notify.error('Error', 'Sorry an error has occured. Please try again later');
        }
        this.notify.warn('Warning', 'An error occured.\nPlease Check your credentials and try again', { type: 'warn', subType: 'warn' });
      });
  }
}
