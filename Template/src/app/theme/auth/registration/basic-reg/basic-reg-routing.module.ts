import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {BasicRegComponent} from './basic-reg.component';

const routes: Routes = [
  {
    path: 'parent',
    component: BasicRegComponent,
    data: {
      title: 'Parent Sign Up'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BasicRegRoutingModule { }
