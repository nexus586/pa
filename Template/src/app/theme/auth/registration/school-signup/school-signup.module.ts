import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SchoolSignupRoutingModule } from './school-signup-routing.module';
import { SchoolSignupComponent } from './school-signup.component';

import {SharedModule} from '../../../../shared/shared.module';
import { FormsModule, ReactiveFormsModule } from "@angular/forms";

import { HttpModule } from "@angular/http";
import { AuthService } from "../../auth.service";
import {SimpleNotificationsModule} from 'angular2-notifications';

@NgModule({
  imports: [
    CommonModule,
    SchoolSignupRoutingModule,
    SharedModule, 
    FormsModule, 
    ReactiveFormsModule, 
    HttpModule, 
    SimpleNotificationsModule.forRoot()
  ],
  declarations: [SchoolSignupComponent],
  providers: [AuthService]
})
export class SchoolSignupModule { }
