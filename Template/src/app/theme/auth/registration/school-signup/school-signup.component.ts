import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, FormControl, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import { CustomValidators } from "ng2-validation";
import { AuthService } from "../../auth.service";
import { NotificationsService } from "angular2-notifications";
import { AppConstants } from "../../../../shared/AppConstants";

@Component({
  selector: 'app-school-signup',
  templateUrl: './school-signup.component.html',
  styleUrls: ['./school-signup.component.scss']
})
export class SchoolSignupComponent implements OnInit {

  form: FormGroup;
  email = new FormControl('', [Validators.required,Validators.email]);
  name = new FormControl('', Validators.required);
  phone = new FormControl('', [Validators.required, CustomValidators.number]);
  password = new FormControl('', [Validators.required, Validators.minLength(6)]);
  rpassword = new FormControl('', [Validators.required, Validators.minLength(6), CustomValidators.equalTo(this.password)]);
  check = new FormControl('', Validators.required);

  user: any;

  notificationOptions: any = AppConstants.notificationConstants;

  constructor(private fb: FormBuilder, private router: Router, private service: AuthService, private noti: NotificationsService) { }

  ngOnInit() {
    this.form = this.fb.group({
      email: this.email,
      name: this.name, 
      phone: this.phone,
      password: this.password,
      rpassword: this.rpassword,
      check: this.check
    });
  }



  onSubmit(){
    this.user = {
      name: this.form.value.name,
      email: this.form.value.email,
      phone: this.form.value.phone,
      password: this.form.value.password
    }  

    this.noti.info('Sign Up', 'Please wait while we create your account', {
      timeOut: 6000,
      showProgressBar: false,
      pauseOnHover: false,
    })

    this.service.registerSchool(JSON.stringify(this.user)).subscribe(
      (data) =>{
        this.noti.success('Success', 'Account created successfully');
        localStorage.setItem('user', JSON.stringify(data.user));
        localStorage.setItem('name', data.name);
        localStorage.setItem('roles', JSON.stringify(data.roles[0].authority));
        localStorage.setItem('token', data.token);
        localStorage.setItem('isLoggedIn', 'true');
        setTimeout(()=>{
          this.router.navigate(['/', 'user']);
        }, 2000)
      },
      (error) =>{
        this.noti.error(error.status, error.reason);
        this.form.reset();
      }
    )
  }

}
