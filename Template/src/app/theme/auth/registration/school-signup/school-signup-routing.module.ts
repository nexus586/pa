import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { SchoolSignupComponent } from "./school-signup.component";

const routes: Routes = [
  {
    path: 'school',
    component: SchoolSignupComponent,
    data: {
      title: 'School Sign Up'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SchoolSignupRoutingModule { }
