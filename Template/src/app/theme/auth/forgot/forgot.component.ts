import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';
import { AppConstants } from '../../../shared/AppConstants';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { NotificationsService } from 'angular2-notifications';

@Component({
  selector: 'app-forgot',
  templateUrl: './forgot.component.html',
  styleUrls: ['./forgot.component.scss']
})
export class ForgotComponent implements OnInit {

  constructor(private router: Router,
    private service: AuthService,
    private fb: FormBuilder,
    private ns: NotificationsService
  ) { }

  email = new FormControl('', [Validators.required, Validators.email]);

  emailObject: any;

  notiOptions = AppConstants.notificationConstants;

  form: FormGroup;

  ngOnInit() {
    this.form = this.fb.group({
      email: this.email
    });
  }

  onSubmit() {
    this.emailObject = {
      email: this.form.value.email
    };

    const object = JSON.stringify(this.emailObject);

    this.ns.info('Please Wait', 'We"re confirming your email');

    this.service.forgotPassword(object).subscribe(
      (data) => {
        this.ns.success('Success', 'Email confirmed successfully. Proceed to change password');
        localStorage.setItem('email', data.email);
        setTimeout(() => {
          this.router.navigate(['/', 'auth', 'change']);
        }, 2000);
      },
      (error) => {
        console.log('[error] =>', error);
      }
    );
  }

}
