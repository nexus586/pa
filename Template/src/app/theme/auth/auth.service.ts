import { Injectable } from '@angular/core';
import { Http, Headers, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Router } from '@angular/router';
import 'rxjs/Rx';

const headers = new Headers({ 'Content-Type': 'application/json' });

@Injectable()
export class AuthService {

  constructor(private http: Http, private router: Router) { }


  login(form: any) {
    return this.http.post('http://localhost:8080/payapp/user/login', form, { headers })
      .map(response => response.json())
      .catch((error) => Observable.throw(error));
  }

  logout(): void {
    localStorage.clear();
    this.router.navigate(['/', 'auth', 'login']);
  }

  registerParent(form: any) {
    return this.http.post('http://localhost:8080/payapp/parent/signUp', form, { headers })
      .map(response => response.json())
      .catch((error) => Observable.throw(error));
  }

  registerSchool(form: any) {
    return this.http.post('http://localhost:8080/payapp/school/signUp', form, { headers })
      .map(response => response.json())
      .catch(error => Observable.throw(error));
  }

  forgotPassword(user: any) {
    return this.http.post('http://localhost:8080/payapp/user/forgotPasswordIndex', user, { headers })
      .map(response => response.json())
      .catch(error => Observable.throw(error));
  }

  findUser(id: number) {
    const userHeader = new Headers({ 'Content-Type': 'application/json', 'Authorization': localStorage.getItem('token') });
    return this.http.get(`http://localhost:8080/payapp/user/getUserById?id=${id}`, { headers: userHeader })
      .map(response => response.json())
      .catch(error => Observable.throw(error));
  }

  updateUser(id: number, user: any) {
    const userHeader = new Headers({ 'Content-Type': 'application/json', 'Authorization': localStorage.getItem('token') });
    return this.http.put(`http://localhost:8080/payapp/user/editUser?id=${id}`, user, { headers: userHeader })
      .map(response => response.json())
      .catch(error => Observable.throw(error));
  }

  changePassword(user: any) {
    return this.http.post('http://localhost:8080/payapp/user/changePassword', user, { headers })
      .map(response => response.json())
      .catch(error => Observable.throw(error));
  }
}
