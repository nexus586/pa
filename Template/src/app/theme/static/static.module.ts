import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StaticRoutingModule } from './static-routing.module';
import { NavComponent } from './nav/nav.component';
import { FooterComponent } from './footer/footer.component';
import { HomeComponent } from './home/home.component';
import { ContactComponent } from './contact/contact.component';
import { AboutComponent } from './about/about.component';
import { FaqComponent } from './faq/faq.component';
import {NgxCarouselModule} from 'ngx-carousel';
import { SharedModule } from '../../shared/shared.module';
import {HttpModule} from '@angular/http';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { SimpleNotificationsModule } from 'angular2-notifications';


import 'hammerjs';

@NgModule({
  imports: [
    CommonModule,
    StaticRoutingModule,
    NgxCarouselModule,
    SharedModule,
    HttpModule,
    ReactiveFormsModule,
    FormsModule,
    SimpleNotificationsModule.forRoot()
  ],
  declarations: [NavComponent, FooterComponent, HomeComponent, ContactComponent, AboutComponent, FaqComponent]
})
export class StaticModule { }
