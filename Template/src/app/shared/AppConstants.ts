export class AppConstants {
    static notificationConstants: any = {
        position: ['top', 'right'],
        maxStack: 8,
        timeOut: 6000,
        showProgressBar: true,
        pauseOnHover: true,
        lastOnButton: true,
        clickToClose: true,
        preventDuplicates: false,
        theClass: 'bg-c-pink',
        rtl: false,
        animate: 'fromRight'
    };
}